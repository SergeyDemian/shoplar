package ru.lar.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import lombok.NoArgsConstructor;
import ru.lar.shop.common.Common;
import ru.lar.shop.dao.CartDAO;
import ru.lar.shop.dao.OrderDAO;
import ru.lar.shop.services.OrderService;

@Controller
@NoArgsConstructor
public class ShopAdminController {

	private CartDAO cartDAO;
	private OrderDAO orderDAO;

	@Autowired
	public ShopAdminController(OrderDAO orderDAO, CartDAO cartDAO) {
		this.orderDAO = orderDAO;
		this.cartDAO = cartDAO;
	}

	@RequestMapping(value = "/admin/orders/{id}", method = RequestMethod.GET)
	public ModelAndView getOrderByUnique(@PathVariable("id") long id, ModelAndView modelAndView) {
		modelAndView.addObject("order", new OrderService(orderDAO, cartDAO).finById(id));
		modelAndView.addObject("changeOrders", new OrderService(orderDAO, cartDAO).finById(id));
		modelAndView.setViewName("changeOrder");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/orders", method = RequestMethod.GET)
	public ModelAndView getOrders(ModelAndView modelAndView) {
		modelAndView.addObject("orders", new OrderService(orderDAO, cartDAO).getListOrders());
		modelAndView.setViewName("orders");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/orders/{id}", method = RequestMethod.PUT)
	public ModelAndView updateNDSDilevity(@ModelAttribute("changeOrders") Common common, ModelAndView modelAndView) {
		new OrderService(orderDAO, cartDAO).updateShopOrder(common);
		return new ModelAndView("redirect:" + common.getId());
	}

}
