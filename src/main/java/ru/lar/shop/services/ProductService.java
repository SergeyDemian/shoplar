package ru.lar.shop.services;

import java.util.List;
import org.springframework.stereotype.Service;

import ru.lar.shop.dao.ProductDAO;
import ru.lar.shop.model.Product;

@Service
public class ProductService {

	ProductDAO productDAO;

	public ProductService(ProductDAO productDAO) {
		super();
		this.productDAO = productDAO;
	}

	public List<Product> getListProduct() {
		return productDAO.findAll();
	}

}
