package ru.lar.shop.services;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.NoArgsConstructor;
import ru.lar.shop.common.Common;
import ru.lar.shop.dao.CartDAO;
import ru.lar.shop.dao.OrderDAO;
import ru.lar.shop.model.Cart;
import ru.lar.shop.model.ShopOrder;

@Service
@NoArgsConstructor
public class OrderService {

	CartDAO cartDAO;
	OrderDAO orderDAO;

	@Autowired
	public OrderService(OrderDAO orderDAO, CartDAO cartDAO) {
		super();
		this.orderDAO = orderDAO;
		this.cartDAO = cartDAO;
	}

	public void createOrder(Common common) {
		Set<Cart> carts = new HashSet<>();
		common.getMapShopOrders().forEach((k, v) -> carts.add(k));
		ShopOrder order = new ShopOrder(common.getNDS(), common.getDileviry(), new Date());
		orderDAO.save(order);
		new CartService(cartDAO).createCart(carts, order);
	}

	public Common finById(long id) {
		return new Common().getCommonforOrder(orderDAO.findById(id).orElse(null));
	}

	public List<ShopOrder> getListOrders() {
		return orderDAO.findAll();
	}

	public void updateShopOrder(Common common) {
		ShopOrder shopOrder = orderDAO.findById(common.getId()).orElse(null);

		shopOrder.setNds(common.getNDS());
		shopOrder.setDileviry(common.getDileviry());
		orderDAO.save(shopOrder);
	}

}
