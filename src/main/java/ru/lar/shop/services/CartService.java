package ru.lar.shop.services;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.NoArgsConstructor;
import ru.lar.shop.common.Common;
import ru.lar.shop.dao.CartDAO;
import ru.lar.shop.model.Cart;
import ru.lar.shop.model.Product;
import ru.lar.shop.model.ShopOrder;

@Service
@NoArgsConstructor
public class CartService {

	CartDAO cartDAO;

	@Autowired
	public CartService(CartDAO cartDAO) {
		this.cartDAO = cartDAO;
	}

	public void createCart(Set<Cart> carts, ShopOrder shopOrder) {
		carts.forEach(x -> x.setShopOrder(shopOrder));
		cartDAO.saveAll(carts);
	}

	public Common getCart(List<Product> products) {
		List<Cart> carts = new ArrayList<>();
		Map<Product, Long> countOfProduct = new LinkedHashMap<>();
		countOfProduct = products.stream().collect(Collectors.groupingBy(e -> (Product) e, Collectors.counting()));
		countOfProduct.forEach((k, v) -> carts.add(new Cart(k, v)));
		if (carts.isEmpty()) {
			return null;
		} else {
			return new Common().getFullPrice(carts);
		}
	}

}
