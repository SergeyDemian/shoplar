package ru.lar.shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Cart {

	@Column
	@Getter
	@Setter
	private long count;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	@Setter
	private long id;

	@ManyToOne
	@JoinColumn(name = "id_product")
	@Getter
	@Setter
	private Product product;

	@ManyToOne
	@JoinColumn(name = "id_order")
	@Getter
	@Setter
	private ShopOrder shopOrder;

	public Cart(Product product, long count) {
		super();
		this.product = product;
		this.count = count;
	}

	public Cart(ShopOrder shopOrder, Product product, long count) {
		super();
		this.shopOrder = shopOrder;
		this.product = product;
		this.count = count;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cart other = (Cart) obj;
		if (count != other.count)
			return false;
		if (id != other.id)
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (count ^ (count >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id + ", product=" + product + ", count=" + count + "]";
	}
}
