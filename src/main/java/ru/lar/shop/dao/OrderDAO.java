package ru.lar.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ru.lar.shop.model.ShopOrder;

@Repository
public interface OrderDAO extends JpaRepository<ShopOrder, Long> {

}
