package ru.lar.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.lar.shop.model.Cart;

public interface CartDAO extends JpaRepository<Cart, Long> {

}
