<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>Детали заказа</title>
</head>
<body>
	<div class="container">
		<ul class="nav justify-content-end">
			<li class="nav-item"><a class="nav-link active"
				href="/admin/orders">Обратно к списку заказов</a></li>
			<li class="nav-item"><form:form action="/logout" method="post"
					name="logout">
					<a onclick="logout.submit();" class="nav-link" href="#">Выход</a>
				</form:form></li>
		</ul>
	</div>
	<div class="container">
		<form:form action="/admin/orders/${order.id}" method="put"
			name="changeOrders" modelAttribute="changeOrders">
			<table class="table table-hover table-sm">
				<tr class="table-active" align="center">
					<th>Наименование продукта</th>
					<th>Цена</th>
					<th>Кол-во</th>
					<th>Сумма</th>
				</tr>
				<c:forEach items="${order.mapShopOrders}" var="shopOrder">
					<tr align="center">
						<td>${shopOrder.key.product.productName}</td>
						<td>${shopOrder.key.product.price}</td>
						<td>${shopOrder.key.count}</td>
						<td>${shopOrder.value}</td>
					</tr>
				</c:forEach>
				<!-- <tr>
					<td td colspan="3" align="right">Стоимость товаров</td>
					<td align="center">${order.sumAllPriceWithoutNDSAndDileviry}</td>
				</tr>-->
				<tr>
					<td colspan="3" align="right">Доставка:</td>
					<td align="center"><form:input path="dileviry" type="text"
							value="${order.dileviry}" /></td>
				</tr>
				<tr>
					<td colspan="3" align="right">Доставка с учётом НДС
						${order.NDS}%:</td>
					<td align="center">${order.dileviryWithNDS}</td>
				</tr>
				<tr>
					<td colspan="3" align="right">Итого, в том числе НДС <form:input
							path="NDS" type="text" value="${order.NDS}" />%:
					</td>
					<td align="center">${order.fullSumm}</td>
				</tr>
			</table>
			<form:input path="id" type="hidden" value="${order.id}" />
			<button type="submit" form="changeOrders" onclick="shopOrderList"
				class="btn btn-primary btn-lg btn-block">Изменить заказ</button>
		</form:form>
	</div>
</body>
</html>