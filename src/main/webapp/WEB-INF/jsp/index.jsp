<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>Список продуктов</title>
</head>
<body>
	<div class="container" align="right">
		<a onclick="logout.submit();" href="/admin/orders"
			class="badge badge-primary">Войти как админ</a>
	</div>
	<h2 align="center" class="form-signin-heading">Выберите
		необходимые продукты.</h2>
	<div class="container">
		<table class="table table-hover table-sm">
			<thead>
				<tr class="table-active" align="center">
					<th scope="col">№</th>
					<th scope="col">Наименование</th>
					<th scope="col">Цена, руб.</th>
					<th scope="col">Действие</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${products}" var="product">
					<tr align="center">
						<th scope="row">${product.id}</th>
						<td>${product.productName}</td>
						<td>${product.price}</td>
						<td><form:form action="/index" method="post"
								modelAttribute="order" name="order">
								<form:input path="id" type="hidden" value="${product.id}" />
								<form:input path="productName" type="hidden"
									value="${product.productName}" />
								<form:input path="price" type="hidden" value="${product.price}" />
								<input type="submit" value="Добавить в корзину"
									class="btn btn-primary btn-sm">
							</form:form></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="container">
		<input type="submit" value="В корзину"
			class="btn btn-primary btn-lg btn-block"
			onclick="window.location='cart';" />
	</div>
</body>
</html>