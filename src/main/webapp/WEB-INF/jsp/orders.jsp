<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>Список заказов</title>
</head>
<body>
	<%@ include file="templates/menubar.jsp"%>
	<h1 align="center" class="form-signin-heading">Список заказов</h1>
	<div class="container">
		<table class="table table-hover table-sm">
			<tr class="table-active" align="center">
				<td>Номер заказа</td>
				<td>Дата создания</td>
			</tr>
			<c:forEach items="${orders}" var="shopOrder">
				<tr align="center"
					onclick="window.location.href='/admin/orders/${shopOrder.id}'; return false">
					<td>${shopOrder.id}</td>
					<td>${shopOrder.createDate}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>